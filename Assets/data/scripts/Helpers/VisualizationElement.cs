﻿using System;
using UnityEngine;

namespace RedDev.Helpers
{
    /// <summary>
    /// Описывает типы визуализации объектов.
    /// </summary>
    public enum EObjectType
    {
        /// <summary>
        /// Параллепипед.
        /// </summary>
        Box = 0,

        /// <summary>
        /// Сфера.
        /// </summary>
        Sphere = 1
    }

    /// <summary>
    /// Класс реализует визуализацию объектов в редакторе.
    /// </summary>
    [ExecuteInEditMode]
    public class VisualizationElement : MonoBehaviour
    {
        [Tooltip("Тип визуализации объекта")]
        public EObjectType Type = EObjectType.Box;

        [Tooltip("Цвет визуализации объектов")]
        public Color ColorElement = Color.cyan;

        void OnDrawGizmos()
        {
            Matrix4x4 matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);

            Gizmos.matrix = matrix;

            Gizmos.color = new Color(ColorElement.r, ColorElement.g, ColorElement.b, 0.25f);

            switch (Type)
            {
                case EObjectType.Box:
                    Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
                    break;
                case EObjectType.Sphere:
                    Gizmos.DrawWireSphere(Vector3.zero, 1.0f);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Gizmos.color = Color.green;

            Gizmos.matrix = Matrix4x4.identity;
        }

        void OnDrawGizmosSelected()
        {
            Matrix4x4 matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);

            Gizmos.matrix = matrix;

            Gizmos.color = ColorElement;

            switch (Type)
            {
                case EObjectType.Box:
                    Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
                    break;
                case EObjectType.Sphere:
                    Gizmos.DrawWireSphere(Vector3.zero, 1.0f);
                    break;
            }

            Gizmos.color = new Color(ColorElement.r, ColorElement.g, ColorElement.b, 0.5f);

            switch (Type)
            {
                case EObjectType.Box:
                    Gizmos.DrawCube(Vector3.zero, Vector3.one);
                    break;
                case EObjectType.Sphere:
                    Gizmos.DrawSphere(Vector3.zero, 1.0f);
                    break;
            }

            Gizmos.color = Color.green;

            Gizmos.matrix = Matrix4x4.identity;
        }
    }
}