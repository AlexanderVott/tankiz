﻿using RedDev.Gameplay.Characters.Tank;
using UnityEngine;

namespace RedDev.Gameplay.UI
{
    /// <summary>
    /// Базовый класс для реализации интерфейса игрока.
    /// </summary>
    public class BaseGameplayUI : MonoBehaviour
    {
        /// <summary>
        /// Цвет активного элемента.
        /// </summary>
        [Tooltip("Цвет активного элемента")]
        public Color ColorPassive = Color.white;

        /// <summary>
        /// Цвет пассивного элемента.
        /// </summary>
        [Tooltip("Цвет пассивного элемента")]
        public Color ColorActive = Color.green;

        /// <summary>
        /// Продолжительность активного состояния.
        /// </summary>
        [Tooltip("Продолжительность активного состояния")]
        public float ActivateTime = 1.5f;

        /// <summary>
        /// Скорость перехода в пассивное состояние.
        /// </summary>
        [Tooltip("Скорость перехода в пассивное состояние")]
        public float SpeedToPassive = 1.0f;

        /// <summary>
        /// Скорость перехода в активное состояние.
        /// </summary>
        [Tooltip("Скорость перехода в активное состояние")]
        public float SpeedToActive = 1.5f;

        protected TankState _state;

        protected void Init()
        {
            GameObject playerObj = GameObject.FindGameObjectWithTag("Player");
            if (playerObj == null)
            {
                Debug.LogError("Для " + gameObject.name + " не был найден объект с тегом Player");
                return;
            }
            _state = playerObj.GetComponent<TankState>();
        }
    }
}