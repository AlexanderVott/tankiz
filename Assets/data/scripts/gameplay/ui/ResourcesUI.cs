﻿using UnityEngine;
using UnityEngine.UI;

namespace RedDev.Gameplay.UI
{
    public class ResourcesUI : BaseGameplayUI
    {
        [Tooltip("Иконка снарядов")]
        public Image BulletsIcon;
        [Tooltip("Текст отображающий текущее количество снарядов выбранного оружия")]
        public Text BulletsText;
        private float _timeActiveBullets;

        [Tooltip("Иконка здоровья")]
        public Image HealthIcon;
        [Tooltip("Текст отображающий текущее значение здоровья")]
        public Text HealthText;
        private float _timeActiveHealth;

        [Tooltip("Иконка отображающая вооружение ТОС")]
        public Image TOSIcon;

        [Tooltip("Иконка отображающая вооружение танковой пушкой")]
        public Image TankIcon;

        [Tooltip("Иконка отображающиая вооружение пулемётом")]
        public Image MachinegunIcon;
        private int _activeWeapon = 0;

        void Start()
        {
            base.Init();
            if (_state != null)
            {
                _state.WeaponController.OnChangeBullets.AddListener(ChangeBullets);
                _state.WeaponController.OnChangeWeapon.AddListener(ChangeWeapon);
                _state.HealthController.OnChangeHealth.AddListener(ChangeHealth);
            }
        }

        void LateUpdate()
        {
            if (Time.time >= _timeActiveBullets + ActivateTime)
            {
                BulletsIcon.color = Color.Lerp(BulletsIcon.color, ColorPassive, Time.deltaTime * SpeedToPassive);
                BulletsText.color = Color.Lerp(BulletsText.color, ColorPassive, Time.deltaTime * SpeedToPassive);
            }
            else
            {
                BulletsIcon.color = Color.Lerp(BulletsIcon.color, ColorActive, Time.deltaTime * SpeedToActive);
                BulletsText.color = Color.Lerp(BulletsText.color, ColorActive, Time.deltaTime * SpeedToActive);
            }

            if (Time.time >= _timeActiveHealth + ActivateTime)
            {
                HealthIcon.color = Color.Lerp(HealthIcon.color, ColorPassive, Time.deltaTime * SpeedToPassive);
                HealthText.color = Color.Lerp(HealthText.color, ColorPassive, Time.deltaTime * SpeedToPassive);
            }
            else
            {
                HealthIcon.color = Color.Lerp(HealthIcon.color, ColorActive, Time.deltaTime * SpeedToActive);
                HealthText.color = Color.Lerp(HealthText.color, ColorActive, Time.deltaTime * SpeedToActive);
            }
            switch (_activeWeapon)
            {
                // machinegun
                case 0:
                    MachinegunIcon.color = ColorActive;
                    TankIcon.color = ColorPassive;
                    TOSIcon.color = ColorPassive;
                    break;
                // tank
                case 1:
                    MachinegunIcon.color = ColorPassive;
                    TankIcon.color = ColorActive;
                    TOSIcon.color = ColorPassive;
                    break;
                // TOS
                case 2:
                    MachinegunIcon.color = ColorPassive;
                    TankIcon.color = ColorPassive;
                    TOSIcon.color = ColorActive;
                    break;
            }
        }
        
        /// <summary>
        /// Метод вызывается в событии при изменнии количества патронов.
        /// </summary>
        /// <param name="bullets">Количество патронов.</param>
        public void ChangeBullets(int bullets)
        {
            _timeActiveBullets = Time.time;
            BulletsText.text = bullets.ToString();
        }

        /// <summary>
        /// Метод вызывается в событии при изменении здоровья.
        /// </summary>
        /// <param name="health">Количество здоровья.</param>
        public void ChangeHealth(int health)
        {
            _timeActiveHealth = Time.time;
            HealthText.text = health.ToString();
        }

        /// <summary>
        /// Метод вызывается при изменении активного оружия.
        /// </summary>
        /// <param name="index">Порядоковый номер оружия.</param>
        public void ChangeWeapon(int index)
        {
            _activeWeapon = index;
        }
    }
}