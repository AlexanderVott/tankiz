﻿using UnityEngine;
using UnityEngine.UI;

namespace RedDev.Gameplay.UI
{
    public class KillsUI : BaseGameplayUI
    {
        [Tooltip("Текст отображающий количество уничтоженных противников")]
        public Text KillsText;
        private float _timeActiveKills = 0.0f;

        void Start()
        {
            base.Init();
            if (_state != null)
            {
                _state.OnChangeKillsCount.AddListener(ChangeKillsCounter);
            }
        }
        
        void LateUpdate()
        {
            if (Time.time >= _timeActiveKills + ActivateTime)
                KillsText.color = Color.Lerp(KillsText.color, ColorPassive, Time.deltaTime * SpeedToPassive);
            else
                KillsText.color = Color.Lerp(KillsText.color, ColorActive, Time.deltaTime * SpeedToActive);
        }

        /// <summary>
        /// Метод реализует событие изменения счётчика уничтоженных противников.
        /// </summary>
        /// <param name="count">Количество уничтоженных противников.</param>
        private void ChangeKillsCounter(int count)
        {
            _timeActiveKills = Time.time;
            KillsText.text = count.ToString();
        }
    }
}