﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RedDev.Gameplay.UI
{
    public class GameStateUI : BaseGameplayUI
    {
        [Tooltip("Панель для демонстрации победы игрока")]
        public Image PanelWin;

        [Tooltip("Панель для демонстрации проигрыша игрока")]
        public Image PanelFail;

        [Tooltip("Текст отображения временных результатов")]
        public Text ResultText;

        [Tooltip("Кнопка для перезапуска уровня")]
        public Button RestartButton;

        void Start()
        {
            base.Init();
            PanelWin.gameObject.SetActive(false);
            PanelFail.gameObject.SetActive(false);
            if (_state != null)
            {
                _state.OnWinEvent.AddListener(() =>
                {
                    PanelWin.gameObject.SetActive(true);
                });
                _state.OnFailEvent.AddListener(time =>
                {
                    PanelFail.gameObject.SetActive(true);
                    TimeSpan timeSpan = TimeSpan.FromSeconds(time);
                    DateTime dateTime = DateTime.Today.Add(timeSpan);
                    ResultText.text += " " + dateTime.ToString("mm:ss");
                });
            }
            RestartButton.onClick.AddListener(() =>
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().path);
            });
        }
    }
}