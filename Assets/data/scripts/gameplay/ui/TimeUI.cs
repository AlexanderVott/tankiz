﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RedDev.Gameplay.UI
{
    public class TimeUI : BaseGameplayUI
    {
        [Tooltip("Текст отображающий время игровой сессии")]
        public Text TimeText;
        private float _timeActiveTimeText = 0.0f;

        void Start()
        {
            base.Init();
            if (_state != null)
            {
                _state.OnTimeChange.AddListener(ChangeTime);
            }
        }

        void LateUpdate()
        {
            if (Time.time >= _timeActiveTimeText + ActivateTime)
                TimeText.color = Color.Lerp(TimeText.color, ColorPassive, Time.deltaTime * SpeedToPassive);
            else
                TimeText.color = Color.Lerp(TimeText.color, ColorActive, Time.deltaTime * SpeedToActive);
        }

        /// <summary>
        /// Метод реализующий отображение изменного значения времени.
        /// </summary>
        /// <param name="time">Текущее значение времени игровой сессии.</param>
        private void ChangeTime(float time)
        {
            _timeActiveTimeText = Time.time;
            TimeSpan timeSpan = TimeSpan.FromSeconds(time);
            DateTime dateTime = DateTime.Today.Add(timeSpan);
            TimeText.text = dateTime.ToString("mm:ss");
        }
    }
}