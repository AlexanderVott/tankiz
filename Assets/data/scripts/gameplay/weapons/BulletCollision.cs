﻿using RedDev.Gameplay.Characters;
using UnityEngine;

namespace RedDev.Gameplay.Weapons
{
    /// <summary>
    /// Класс описывает реакцию снаряда на коллизию.
    /// </summary>
    [RequireComponent(typeof(Bullet))]
    public class BulletCollision : MonoBehaviour
    {
        private Bullet _bullet;

        /// <summary>
        /// Префаб создаваемый при столкновении снаряда с другими объектами.
        /// </summary>
        [Tooltip("Префаб создаваемый при столкновении снаряда с другими объектами")]
        public GameObject PrefabCollision;

        /// <summary>
        /// Радиус поражения снаряда.
        /// </summary>
        [Tooltip("Радиус поражения снаряда")]
        public float RadiusDamage = 0.0f;

        void Start()
        {
            _bullet = GetComponent<Bullet>();
        }
        
        public void DamageCollision(Collision collision)
        {
            if (RadiusDamage <= 0.01f)
            {
                DamageObject(collision.gameObject);
            }
            else
            {
                Collider[] colliders = Physics.OverlapSphere(transform.position, RadiusDamage);
                for (int i = 0; i < colliders.Length; i++)
                    DamageObject(colliders[i].gameObject);
            }
        }

        private void DamageObject(GameObject gObject)
        {
            if (!gObject.tag.Equals("Enemy"))
                return;
            Health health = gObject.GetComponent<Health>();
            if (health != null)
                health.Damage(_bullet.Damage);
        }

        /// <summary>
        /// Метод обеспечивает создание объекта после начала обработки коллизии.
        /// </summary>
        public void MakeObjectAfterCollision()
        {
            if (PrefabCollision != null)
                Instantiate(PrefabCollision, transform.position, Quaternion.identity);
        }
    }
}