﻿using RedDev.Helpers;
using UnityEngine;
using UnityEngine.Events;

namespace RedDev.Gameplay.Characters
{
    /// <summary>
    /// Контроллер отвечает за здровье персонажа.
    /// </summary>
    public class Health : BaseController
    {
        [Tooltip("Максимальное количество здоровья персонажа")]
        public int HealthMax = 100;

        private float _currentHealth = 0.0f;
        /// <summary>
        /// Текущее значение здоровья.
        /// </summary>
        public float CurrentHealth {
            get { return _currentHealth; }
            set
            {
                _currentHealth = value;
                if (OnChangeHealth != null)
                    OnChangeHealth.Invoke(Mathf.RoundToInt(value));
            }
        }

        public EventInt OnChangeHealth;

        [Tooltip("Принимает значение от 0 до 1")]
        public float Armor;

        private float _armor;

        [Tooltip("Событие вызываемое при смерти персонажа")]
        public UnityEvent OnDead;

        private bool _eventDeadCalled = false;

        [Tooltip("Префа создаваемый после смерти объекта")]
        public GameObject PrefabAfterDead;

        private bool _isDead;
        public bool IsDead
        {
            get { return _isDead; }
        }

        public override void Init()
        {
            base.Init();
            CurrentHealth = HealthMax;
            _isDead = false;
            _armor = 1.0f - Armor;
            _eventDeadCalled = false;
        }

        /// <summary>
        /// Метод обеспечивает возможность нанесения урона.
        /// </summary>
        /// <param name="damage">Величина наносимого урона.</param>
        public void Damage(int damage)
        {
            CurrentHealth -= (damage * _armor);
            _isDead = CurrentHealth <= 0;
            if (_isDead && OnDead != null)
                Dead();
            if (OnChangeHealth != null)
                OnChangeHealth.Invoke(Mathf.RoundToInt(_currentHealth));
        }

        /// <summary>
        /// Метод создаёт указанный объект после смерти персонажа.
        /// </summary>
        public void MakeObjectAfterDead()
        {
            if (PrefabAfterDead != null)
                Instantiate(PrefabAfterDead, transform.position, Quaternion.identity);
        }

        /// <summary>
        /// Реализует смерть персонажа.
        /// </summary>
        public void Dead()
        {
            MakeObjectAfterDead();
            if (OnDead != null && !_eventDeadCalled)
            {
                _eventDeadCalled = true;
                OnDead.Invoke();
            }
        }
    }
}