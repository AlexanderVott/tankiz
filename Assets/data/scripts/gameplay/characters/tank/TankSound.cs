﻿using UnityEngine;

namespace RedDev.Gameplay.Characters.Tank
{
    /// <summary>
    /// Класс реализует работу со звуковыми эффектами танка.
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class TankSound : BaseController
    {
        /// <summary>
        /// Источник звука двигателя танка.
        /// </summary>
        [Tooltip("Источник звука двигателя танка")]
        public AudioSource MovementSource;

        /// <summary>
        /// Диапазон изменения тона звучания двигателя.
        /// </summary>
        [Tooltip("Диапазон изменения тона звучания двигателя")]
        public float PitchRange;

        /// <summary>
        /// Аудиоклип звука холостой работы двигателя.
        /// </summary>
        [Tooltip("Аудиоклип звука холостой работы двигателя")]
        public AudioClip IdleClip;

        /// <summary>
        /// Аудиоклип звука активной работы двигателя.
        /// </summary>
        [Tooltip("Аудиоклип звука активной работы двигателя")]
        public AudioClip MovingClip;

        private float _originalPitch;

        public override void Init()
        {
            base.Init();
            if (MovementSource == null)
                MovementSource = GetComponent<AudioSource>();
            _originalPitch = MovementSource.pitch;
        }
        
        /// <summary>
        /// Метод изменяет тон звучания двигателя на случайное значение в указанном диапазоне (<see cref="PitchRange"/>).
        /// </summary>
        private void RandomRangePitch()
        {
            MovementSource.pitch = Random.Range(_originalPitch - PitchRange, _originalPitch + PitchRange);
        }

        /// <summary>
        /// Включает звук холостой работы двигателя.
        /// </summary>
        public void EnableIdleSound()
        {
            if (MovementSource.clip != MovingClip)
            {
                MovementSource.clip = MovingClip;
                RandomRangePitch();
                MovementSource.Play();
            }
        }

        /// <summary>
        /// Включает звук активной работы двигателя.
        /// </summary>
        public void EnableMovingSound()
        {
            if (MovementSource.clip != IdleClip)
            {
                MovementSource.clip = IdleClip;
                RandomRangePitch();
                MovementSource.Play();
            }
        }
    }
}