﻿using RedDev.Gameplay.Icons;
using RedDev.Gameplay.Weapons;
using RedDev.Helpers;
using UnityEngine;
using UnityEngine.Events;

namespace RedDev.Gameplay.Characters.Tank
{
    /// <summary>
    /// Контроллер состояний танка.
    /// </summary>
    [RequireComponent(typeof(TankMovement))]
    [RequireComponent(typeof(TankSound))]
    [RequireComponent(typeof(WeaponController))]
    [RequireComponent(typeof(Health))]
    public class TankState : MonoBehaviour
    {
        /// <summary>
        /// Контроллер перемещения.
        /// </summary>
        public TankMovement TankMovement { get; private set; }

        /// <summary>
        /// Контроллер звука танка.
        /// </summary>
        public TankSound SoundController { get; private set; }

        /// <summary>
        /// Контроллер оружия.
        /// </summary>
        public WeaponController WeaponController { get; private set; }

        /// <summary>
        /// Контроллер здоровья.
        /// </summary>
        public Health HealthController { get; private set; }

        private float _seconds;

        /// <summary>
        /// Время с момента начала игровой сессии (момент появления танка, в секундах).
        /// </summary>
        public float Seconds {
            get { return _seconds; }
            private set
            {
                _seconds = value;
                if (OnTimeChange != null)
                    OnTimeChange.Invoke(value);
            }
        }
        private float _startTime;

        /// <summary>
        /// Событие вызываемое при изменении значения времени игровой сессии.
        /// </summary>
        public EventFloat OnTimeChange;

        private int _countKills = 0;
        
        /// <summary>
        /// Количество уничтоженных противников.
        /// </summary>
        public int CountsKills
        {
            get { return _countKills; }
        }

        /// <summary>
        /// Событие вызываемое при изменении числа уничтоженных противников.
        /// </summary>
        [Tooltip("Событие вызываемое при изменении числа уничтоженных противников")]
        public EventInt OnChangeKillsCount;

        /// <summary>
        /// Событие вызываемое при выполнении условий победы. В данной сборке игроку необходимо продержаться 60 минут.
        /// </summary>
        [Tooltip("Событие вызываемое при выполнении условий победы.\nВ данной сборке игроку необходимо продержаться 60 минут.")]
        public UnityEvent OnWinEvent;

        /// <summary>
        /// Событие вызываемое при поражении игрока.
        /// В данной сборке при здоровье равном нулю.
        /// </summary>
        [Tooltip("Событие вызываемое при поражении игрока.\nВ данной сборке при здоровье равном нулю.")]
        public EventFloat OnFailEvent;

        private bool _eventCalled = false;

        void Awake()
        {
            TankMovement = GetComponent<TankMovement>();
            TankMovement.Init();
            SoundController = GetComponent<TankSound>();
            SoundController.Init();
            WeaponController = GetComponent<WeaponController>();
            WeaponController.Init();
            HealthController = GetComponent<Health>();
            HealthController.Init();
        }

        void Start()
        {
            _startTime = Time.time;
        }

        // Update is called once per frame
        void Update()
        {
            TankMovement.Tick();
            WeaponController.Tick();
            EngineAudio();

            if (!HealthController.IsDead)
                Seconds = Time.time - _startTime;
            else if (_seconds > 3600.0f && OnWinEvent != null && !_eventCalled)
            {
                _eventCalled = true;
                OnWinEvent.Invoke();
            }
            if (HealthController.IsDead && !_eventCalled)
            {
                _eventCalled = true;
                OnFailEvent.Invoke(Seconds);
            }
        }

        void FixedUpdate()
        {
            TankMovement.FixedTick();
            WeaponController.FixedTick();
        }
        
        void OnTriggerEnter(Collider collider)
        {
            BulletsRecovery bulletsRecovery = collider.gameObject.GetComponent<BulletsRecovery>();
            if (bulletsRecovery != null)
            {
                if (bulletsRecovery.WeaponName.Equals("HEALTH"))
                    HealthController.CurrentHealth = HealthController.HealthMax;
                else
                    WeaponController.RestoreBullets(bulletsRecovery.WeaponName);

                if (bulletsRecovery.EventTrigger != null)
                    bulletsRecovery.EventTrigger.Invoke();
            }
        }

        /// <summary>
        /// Метод реализует перемещение танка.
        /// </summary>
        /// <param name="value">Значение ввода, регулирует направление вперёд-назад.</param>
        public void Moving(float value)
        {
            TankMovement.MoveInput = value;
        }

        /// <summary>
        /// Метод реализует вращение танка.
        /// </summary>
        /// <param name="value">Значение ввода, регулирует направление влево-вправо.</param>
        public void Turning(float value)
        {
            TankMovement.TurnInput = value;
        }

        /// <summary>
        /// Метод реализует переключение звука двигателя в зависимости от состояния движения.
        /// </summary>
        private void EngineAudio()
        {
            if (Mathf.Abs(TankMovement.MoveInput) < 0.1f && Mathf.Abs(TankMovement.TurnInput) < 0.1f)
                SoundController.EnableMovingSound();
            else
                SoundController.EnableIdleSound();
        }

        /// <summary>
        /// Метод выполняет стрельбу танка.
        /// </summary>
        /// <param name="triggered">Указывает зажата ли клавиша, или отпущена.</param>
        public void Fire(bool triggered)
        {
            WeaponController.Fire(triggered);
        }

        /// <summary>
        /// Увеличивает количество уничтоженных противников на 1.
        /// </summary>
        public void AddKill()
        {
            _countKills++;
            if (OnChangeKillsCount != null)
                OnChangeKillsCount.Invoke(_countKills);
        }
    }
}