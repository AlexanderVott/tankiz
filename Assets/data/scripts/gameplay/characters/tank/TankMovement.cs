﻿using UnityEngine;

namespace RedDev.Gameplay.Characters.Tank
{
    /// <summary>
    /// Класс реализует функционал перемещения танка.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class TankMovement : BaseController
    {
        private Rigidbody _rigidbody;

        /// <summary>
        /// Ссылка на компонент физического тела.
        /// </summary>
        public Rigidbody RigidBody
        {
            get
            {
                return _rigidbody;
            }
        }

        /// <summary>
        /// Скорость перемещения.
        /// </summary>
        [Tooltip("Скорость перемещения персонажа")]
        public float Speed = 10;

        /// <summary>
        /// Скорость вращения.
        /// </summary>
        [Tooltip("Скорость вращения персонажа")]
        public float TurnSpeed = 90;

        /// <summary>
        /// Вводимый параметр, регулирующий перемещение вперёд-назад.
        /// </summary>
        public float MoveInput { get; set; }

        /// <summary>
        /// Вводимый параметр, регулирующий вращение влево-вправо.
        /// </summary>
        public float TurnInput { get; set; }

        public override void Init()
        {
            base.Init();
            _rigidbody = GetComponent<Rigidbody>();
            if (_rigidbody == null)
                Debug.LogError("У персонажа отсутствует компонент RigidBody: " + gameObject.name);
        }

        public override void Tick()
        {
            Vector3 movement = transform.forward * MoveInput * Speed * Time.deltaTime;
            _rigidbody.MovePosition(_rigidbody.position + movement);

            float turn = TurnInput * TurnSpeed * Time.deltaTime;
            Quaternion turnQ = Quaternion.Euler(0.0f, turn, 0.0f);
            _rigidbody.MoveRotation(transform.rotation * turnQ);
        }
    }
}