﻿using UnityEngine;
using UnityEngine.AI;

namespace RedDev.Gameplay.Characters
{
    /// <summary>
    /// Класс реализует простейшее перемещение с использованием стандартных возможностей для поиска пути.
    /// </summary>
    [RequireComponent(typeof(NavMeshAgent))]
    public class MovementController : BaseController
    {
        /// <summary>
        /// Контроллер навигации.
        /// </summary>
        public NavMeshAgent NavController { get; private set; }

        public override void Init()
        {
            base.Init();
            NavController = GetComponent<NavMeshAgent>();
        }

        /// <summary>
        /// Метод реализует перемещение к указанной точке.
        /// </summary>
        /// <param name="point">Координаты куда необходимо переместиться.</param>
        public void MoveTo(Vector3 point)
        {
            NavController.SetDestination(point);
        }
    }
}