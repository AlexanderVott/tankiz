﻿using RedDev.Helpers;
using UnityEngine;

namespace RedDev.Gameplay.Characters.Enemies.AI
{
    /// <summary>
    /// Класс описывает простейшую логику поведения противника.
    /// </summary>
    [RequireComponent(typeof(EnemyState))]
    public class AIEnemy : MonoBehaviour
    {
        private EnemyState _state;

        /// <summary>
        /// Урон наносимый при коллизии.
        /// </summary>
        [Tooltip("Урон наносимый при коллизии")]
        public int Damage = 0;

        /// <summary>
        /// Радиус поражения.
        /// </summary>
        [Tooltip("Радиус поражения")]
        public int RadiusDamage = 0;

        private Transform _player;

        /// <summary>
        /// Объект создаваемый при начале обработки коллизии.
        /// </summary>
        [Tooltip("Объект создаваемый при начале обработки коллизии")]
        public GameObject PrefabCollision;

        private bool _damaged = false;

        /// <summary>
        /// Событие вызываемое, если противник столкнулся с игроком.
        /// </summary>
        [Tooltip("Событие вызываемое, если противник столкнулся с игроком")]
        public EventCollision EventPlayerCollision;

        void Start()
        {
            _state = GetComponent<EnemyState>();
            GameObject gobjPlayer = GameObject.FindGameObjectWithTag("Player");
            if (gobjPlayer != null)
                _player = gobjPlayer.transform;
            else
                Debug.LogError("Не был найден объект игрока для " + gameObject.name);
        }

        void FixedUpdate()
        {
            if (_player != null && !_state.HealthController.IsDead)
                _state.MovementController.MoveTo(_player.position);
            else
                _state.MovementController.MoveTo(transform.position);
        }

        void OnCollisionStay(Collision collision)
        {
            if (collision.gameObject.tag.Equals("Player") && !_damaged)
            {
                _damaged = true;
                if (RadiusDamage == 0)
                {
                    DamageObject(collision.gameObject);
                }
                else
                {
                    Collider[] colliders = Physics.OverlapSphere(transform.position, RadiusDamage);
                    for (int i = 0; i < colliders.Length; i++)
                        DamageObject(colliders[i].gameObject);
                }
                if (PrefabCollision != null)
                    Instantiate(PrefabCollision, transform.position, Quaternion.identity);
                if (EventPlayerCollision != null)
                    EventPlayerCollision.Invoke(collision);
            }
        }

        void OnCollisionExit(Collision collision)
        {
            if (collision.gameObject.tag.Equals("Player"))
                _damaged = false;
        }

        /// <summary>
        /// Повреждает указанный объект, если у него присутствует компонент <see cref="Health"/>.
        /// </summary>
        /// <param name="gobj">Объект, которому необходимо нанести повреждение.</param>
        private void DamageObject(GameObject gobj)
        {
            Health health = gobj.gameObject.GetComponent<Health>();
            if (health != null)
                health.Damage(Damage);
        }
    }
}