﻿using System.Collections;
using RedDev.Helpers;
using UnityEngine;

namespace RedDev.Gameplay.Characters.Enemies
{
    /// <summary>
    /// Класс реализует управление состояними и контроллеами противника.
    /// </summary>
    [RequireComponent(typeof(MovementController))]
    [RequireComponent(typeof(Health))]
    public class EnemyState : ObjectPool
    {
        /// <summary>
        /// Контроллер анимации.
        /// </summary>
        public Animator AnimationController { get; private set; }

        /// <summary>
        /// Контроллер перемещений.
        /// </summary>
        public MovementController MovementController { get; private set; }

        /// <summary>
        /// Контроллер здоровья.
        /// </summary>
        public Health HealthController { get; private set; }

        /// <summary>
        /// Поле необходимое для фабрики, указывает, что необходима первичная настройка.
        /// </summary>
        [HideInInspector]
        public bool FirstInitialize = true;

        void Start()
        {
            Create();
        }

        public override void Create()
        {
            base.Create();
            StopAllCoroutines();
            if (AnimationController == null)
                AnimationController = GetComponentInChildren<Animator>();
            if (AnimationController != null)
            {
                AnimationController.ResetTrigger("Die");
                AnimationController.SetBool("Dead", false);
            }
            if (MovementController == null)
                MovementController = GetComponent<MovementController>();
            MovementController.Init();
            if (HealthController == null)
                HealthController = GetComponent<Health>();
            HealthController.Init();
        }

        void FixedUpdate()
        {
            if (HealthController.IsDead)
                AnimationController.SetBool("Die", true);
        }

        /// <summary>
        /// Метод реализует уничтожение персонажа через указанное время.
        /// </summary>
        public void DeadTimer()
        {
            StartCoroutine(PushTimerExecute());
        }

        /// <summary>
        /// Метод реализует задержку перед уничтожением персонажа на указанное время.
        /// </summary>
        /// <returns></returns>
        private IEnumerator PushTimerExecute()
        {
            // HACK: жёстко указанное значение паузы перед уничтожением объекта.
            yield return new WaitForSeconds(2.0f);
            Push();
        }
    }
}