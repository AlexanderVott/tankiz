﻿using UnityEngine;

namespace RedDev.Gameplay
{
    public class BaseController : MonoBehaviour
    {
        /// <summary>
        /// Базовый метод инициализации контроллера.
        /// </summary>
        public virtual void Init()
        {
            Debug.Log("Initialize " + gameObject.name + " - " + this.GetType().Name + "...");
        }

        /// <summary>
        /// Базовый метод обновления состояния контроллера.
        /// </summary>
        public virtual void Tick() { }

        /// <summary>
        /// Базовый метод обновления состояния контроллера в фиксированным интервалом.
        /// </summary>
        public virtual void FixedTick() { }
    }
}