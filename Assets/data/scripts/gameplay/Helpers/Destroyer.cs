﻿using UnityEngine;

namespace RedDev.Gameplay.Helpers
{
    /// <summary>
    /// Класс обеспечивает ничтонижение объектов при определённых условиях.
    /// </summary>
    public class Destroyer : MonoBehaviour
    {
        [Tooltip("Время после которого объект самоуничтожится.\nЕсли значение равно нулю, то данный метод отключается.")]
        public int SecondsForDestroy = 0;

        void Start()
        {
            if (SecondsForDestroy > 0)
                Destroy(gameObject, SecondsForDestroy);
        }

        /// <summary>
        /// Уничтожает объект сразу.
        /// </summary>
        /// <param name="obj"></param>
        public void DestroyObject(GameObject obj)
        {
            Destroy(obj);
        }

        /// <summary>
        /// Уничтожает объект через указанное время.
        /// </summary>
        /// <param name="time">Время (секунды) через которое необходимо уничтожить объект.</param>
        public void DestroySelfByTime(float time)
        {
            DestroyObject(gameObject, time);
        }

        /// <summary>
        /// Уничтожает объект, к которому прикреплен скрипт.
        /// </summary>
        public void DestroySelf()
        {
            DestroyObject(gameObject);
        }
    }
}