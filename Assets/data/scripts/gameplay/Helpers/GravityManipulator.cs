﻿using UnityEngine;

namespace RedDev.Gameplay.Helpers
{
    /// <summary>
    /// Класс обеспечивает работу с параметром физического тела.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class GravityManipulator : MonoBehaviour
    {
        private Rigidbody _body;
        
        /// <summary>
        /// Метод обеспечивает переключение учёта гравитации и обратного переключения использования кинетического перемещения.
        /// </summary>
        /// <param name="use">В случае получения true включается учёт гравитации и отключение кинетического перемещения.</param>
        public void ChangeUseGravity(bool use)
        {
            if (_body == null)
                _body = GetComponent<Rigidbody>();
            _body.useGravity = use;
            _body.isKinematic = !use;
        }

        /// <summary>
        /// Включает использование гравитации и отключает кинетич. перемещения при срабатывании события с использованием коллизии.
        /// </summary>
        /// <param name="collision"></param>
        public void EnableGravityBeforeCollision(Collision collision)
        {
            ChangeUseGravity(true);
        }

        /// <summary>
        /// Выключает использование гравитации и включает кинетич. перемещения при срабатывании события с использованием коллизии.
        /// </summary>
        /// <param name="collision"></param>
        public void DisableGravityBeforeCollision(Collision collision)
        {
            ChangeUseGravity(false);
        }
    }
}