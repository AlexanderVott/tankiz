﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedDev.Gameplay.Helpers
{
    /// <summary>
    /// Класс реализует простейшую фабрику создания объектов в указанных точках.
    /// </summary>
    /// <typeparam name="T">Тип объекта, которые необходимо создавать в указанных точках.</typeparam>
    public class BaseManufact<T> : MonoBehaviour where T : MonoBehaviour
    {
        /// <summary>
        /// Список используемых точек респауна.
        /// </summary>
        protected List<Transform> _objectsUsed = new List<Transform>();

        /// <summary>
        /// Список точек респауна и в процессе выполнения список неиспользуемых в данный момент точек. 
        /// </summary>
        [Tooltip("Список точек респауна и в процессе выполнения список неиспользуемых в данный момент точек")]
        public List<Transform> ObjectsUnused = new List<Transform>();

        /// <summary>
        /// Массив префабов, которые необходимо создавать.
        /// </summary>
        [Tooltip("Массив префабов, которые необходимо создавать")]
        public T[] ObjectsPrefabs;

        /// <summary>
        /// Время появления объекта.
        /// </summary>
        [Tooltip("Время появления объекта")]
        public int TimeForRespawn = 10;

        /// <summary>
        /// Максимальное количество активных точек респауна.
        /// </summary>
        [Tooltip("Максимальное количество активных точек респауна")]
        public int MaxActiveRespawners = 5;

        /// <summary>
        /// Метод устанавливает точку восстановления снарядов через время <see cref="TimeForRespawn"/> в нужную позицию в пространстве.
        /// </summary>
        /// <param name="transformPoint">Объект, в позицию которого необходимо установить точку восстановления патронов.</param>
        /// <returns></returns>
        protected virtual IEnumerator StartWaiting(Transform transformPoint)
        {
            int index = _objectsUsed.IndexOf(transformPoint);
            ObjectsUnused.Add(_objectsUsed[index]);
            _objectsUsed.RemoveAt(index);

            return null;
        }

        /// <summary>
        /// Метод активирует новую точку восстановления снарядов через время <see cref="TimeForRespawn"/>.
        /// </summary>
        public void StartRespawn()
        {
            // выбираем точку респавна
            int index = Random.Range(0, ObjectsUnused.Count);
            Transform point = ObjectsUnused[index];
            _objectsUsed.Add(point);
            ObjectsUnused.RemoveAt(index);

            StartCoroutine(StartWaiting(point));
        }
    }
}