﻿using RedDev.Helpers;
using UnityEngine;

namespace RedDev.Gameplay.Weapons
{
    /// <summary>
    /// Контроллер оружия.
    /// </summary>
    public class WeaponController : BaseController
    {
        /// <summary>
        /// Массив возможного оружия.
        /// </summary>
        [Tooltip("Массив возможного оружия")]
        public Weapon[] Weapons;

        private int _currentWeapon;

        /// <summary>
        /// Событие вызываемое при смене оружия.
        /// </summary>
        [Tooltip("Событие вызываемое при смене оружия.")]
        public EventInt OnChangeWeapon;

        /// <summary>
        /// Событие вызываемое при изменении количества снарядов.
        /// </summary>
        [Tooltip("Событие вызываемое при изменении количества снарядов")]
        public EventInt OnChangeBullets;

        private int _bulletsCounter = 0;

        public override void Init()
        {
            base.Init();
            if (Weapons.Length == 0)
            {
                Debug.LogWarning("Количество оружия равно 0.");
                Debug.LogWarning("Будет выполнен автомотический поиск...");
                Weapons = GetComponentsInChildren<Weapon>();
                Debug.LogWarning("Количество оружия после поиска: " + Weapons.Length);
            }
            if (Weapons.Length > 0)
                _currentWeapon = 0;
        }

        /// <summary>
        /// Метод реализует переключение оружия по названию.
        /// </summary>
        /// <param name="nameWeapon">Название оружия.</param>
        public void SwitchTo(string nameWeapon)
        {
            for (int i = 0; i < Weapons.Length; i++)
                if (Weapons[i].Name == nameWeapon)
                {
                    SwitchTo(i);
                    break;
                }
        }

        /// <summary>
        /// Метод реализует переключение оружия по порядковому номеру.
        /// </summary>
        /// <param name="index">Порядковый номер в массиве.</param>
        public void SwitchTo(int index)
        {
            if (index >= 0 && index < Weapons.Length)
            {
                Weapons[_currentWeapon].gameObject.SetActive(false);
                _currentWeapon = index;
                Weapons[_currentWeapon].gameObject.SetActive(true);

                _bulletsCounter = Weapons[_currentWeapon].BulletsCount;
                if (OnChangeWeapon != null)
                    OnChangeWeapon.Invoke(_currentWeapon);
                if (OnChangeBullets != null)
                    OnChangeBullets.Invoke(Weapons[_currentWeapon].BulletsCount);
            }
        }

        /// <summary>
        /// Метод обеспечивает переключение оружия далее по списку.
        /// </summary>
        public void NextWeapon()
        {
            if (_currentWeapon + 1 >= Weapons.Length)
                SwitchTo(0);
            else
                SwitchTo(_currentWeapon + 1);
        }

        /// <summary>
        /// Метод обеспечивает переключение оружия в обратном порядке по списку.
        /// </summary>
        public void PrevWeapon()
        {
            if (_currentWeapon - 1 < 0)
                SwitchTo(Weapons.Length - 1);
            else
                SwitchTo(_currentWeapon - 1);
        }

        /// <summary>
        /// Метод реализует стрельбу.
        /// </summary>
        /// <param name="triggered">Указывает зажата ли кнопка выстрела, или отпущена.</param>
        public void Fire(bool triggered)
        {
            if (Weapons.Length > 0)
            {
                Weapons[_currentWeapon].Fire(triggered);
                if (_bulletsCounter != Weapons[_currentWeapon].BulletsCount)
                {
                    _bulletsCounter = Weapons[_currentWeapon].BulletsCount;
                    if (OnChangeBullets != null)
                    {
                        OnChangeBullets.Invoke(_bulletsCounter);
                    }
                }
            }
        }

        /// <summary>
        /// Метод обеспечивает восстановление боезапаса оружия по названию оружия.
        /// </summary>
        /// <param name="nameWeapon">Название оружия, которому необходимо восстановить боезапас.</param>
        public void RestoreBullets(string nameWeapon)
        {
            for (int i = 0; i < Weapons.Length; i++)
            {
                if (Weapons[i].Name.Equals(nameWeapon))
                {
                    Weapons[i].RestoreBullets();
                    break;
                }
            }
        }
    }
}