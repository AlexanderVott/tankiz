﻿using RedDev.Helpers;
using UnityEngine;
using UnityEngine.Events;

namespace RedDev.Gameplay.Weapons
{
    /// <summary>
    /// Класс реализует функционал снаряда.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class Bullet : ObjectPool
    {
        private Rigidbody _body;

        /// <summary>
        /// Событие срабатывает в момент старта снаряда.
        /// </summary>
        [Tooltip("Событие срабатывает в момент старта снаряда")]
        public UnityEvent EventStartBullet;

        /// <summary>
        /// Скорость движения снаряда.
        /// </summary>
        [Tooltip("Скорость движения снаряда")]
        public float Speed = 1.0f;

        /// <summary>
        /// Повреждение здоровья наносимое снарядом.
        /// </summary>
        [Tooltip("Повреждение здоровья наносимое снарядом")]
        public int Damage = 10;

        /// <summary>
        /// Сила применяемая к объекту попадания.
        /// </summary>
        [Tooltip("Сила применяемая к объекту попадания")]
        public float ForceImpact = 500.0f;

        /// <summary>
        /// Направление движения объекта.
        /// Если значения нулевые, берёт значение forward объекта.
        /// </summary>
        [Tooltip("Направление движения объекта.\nЕсли значения нулевые, берёт значение forward объекта")]
        public Vector3 Direction = Vector3.zero;

        /// <summary>
        /// Расстояние прямого выстрела, после чего срабатывает событие <see cref="EventAfterDirectShotDistance"/>
        /// </summary>
        [Tooltip("Расстояние прямого выстрела, после чего срабатывает событие EventAfterDirectShotDistance")]
        public float DirectShotDistance = 100.0f;

        /// <summary>
        /// Событие срабатываемое после преодоления дистанции прямого выстрела.
        /// </summary>
        [Tooltip("Событие срабатываемое после преодоления дистанции прямого выстрела")]
        public UnityEvent EventAfterDirectShotDistance;

        private bool _eventDistanceCalled = false;

        private Collider _collider;

        private float _distanceCounter = 0.0f;

        /// <summary>
        /// Точка с которой стартовал снаряд.
        /// </summary>
        public Vector3 StartPoint { get; set; }

        /// <summary>
        /// Событие срабатываемое при начале столкновения.
        /// </summary>
        [Tooltip("Событие срабатываемое при начале столкновения")]
        public EventCollision EventOnCollisionEnter;

        private float _timerCounter;

        /// <summary>
        /// Таймер для срабатывания события истечения времени.
        /// </summary>
        [Tooltip("Таймер для срабатывания события истечения времени")]
        public float TimeForEvent = 60.0f;

        /// <summary>
        /// Событие срабатывает по истечению времени указанного в <see cref="TimeForEvent"/>.
        /// </summary>
        [Tooltip("Событие срабатывает по истечению времени указанного в TimeForEvent")]
        public UnityEvent EventAfterTime;
        
        public override void Create()
        {
            base.Create();

            if (_body == null)
                _body = GetComponent<Rigidbody>();
            _body.ResetInertiaTensor();

            if (Direction == Vector3.zero)
                Direction = transform.forward;

            if (_collider == null)
                _collider = GetComponent<Collider>();
            if (_collider == null)
                Debug.LogWarning("У снаряда " + gameObject.name + " отсутствует коллизия.");

            _timerCounter = 0.0f;
            _distanceCounter = 0.0f;

            _eventDistanceCalled = false;

            if (EventStartBullet != null)
                EventStartBullet.Invoke();
        }
        
        void FixedUpdate()
        {
            Vector3 movement = Direction * Speed;
            _body.MovePosition(_body.position + movement);
            _distanceCounter = Vector3.Distance(StartPoint, transform.position);
            if (_distanceCounter >= DirectShotDistance && 
                EventAfterDirectShotDistance != null &&
                !_eventDistanceCalled)
            {
                EventAfterDirectShotDistance.Invoke();
                _eventDistanceCalled = true;
            }

            _timerCounter += Time.fixedDeltaTime;
            if (_timerCounter >= TimeForEvent && EventAfterTime != null)
                EventAfterTime.Invoke();
        }

        void OnCollisionEnter(Collision collision)
        {
            if (EventOnCollisionEnter != null)
                EventOnCollisionEnter.Invoke(collision);
        }
    }
}