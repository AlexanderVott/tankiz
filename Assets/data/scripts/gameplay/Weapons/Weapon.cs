﻿using System.Collections;
using RedDev.ManagersEngine;
using UnityEngine;

namespace RedDev.Gameplay.Weapons
{
    /// <summary>
    /// Описывает тип (режим) стрельбы.
    /// </summary>
    public enum EFireType
    {
        /// <summary>
        /// Режим стрельбы одиночными.
        /// </summary>
        Single = 0,

        /// <summary>
        /// Автоматический огонь.
        /// </summary>
        Auto,

        /// <summary>
        /// Стрельба залпом из всех стволов.
        /// </summary>
        Full
    }

    /// <summary>
    /// Класс оружия.
    /// </summary>
    public class Weapon : MonoBehaviour
    {
        /// <summary>
        /// Название оружия.
        /// </summary>
        [Tooltip("Название оружия")]
        public string Name;

        /// <summary>
        /// Тип стрельбы оружия.
        /// </summary>
        [Tooltip("Тип стрельбы оружия")]
        public EFireType FireType;

        private bool _untriggered = false;

        /// <summary>
        /// Количество выстрелов в минуту.
        /// </summary>
        [Tooltip("Количество выстрелов в минуту")]
        public int RateOfFire;

        private float _rateOfFire;

        /// <summary>
        /// Коэффициент разброса.
        /// </summary>
        [Tooltip("Коэффициент разброса")]
        public int SpreadRadius;

        /// <summary>
        /// Количество снарядов в магазине.
        /// </summary>
        [Tooltip("Количество снарядов в магазине")]
        public int MaxBulletsMagazine;

        private int _bulletsCounter = 0;

        /// <summary>
        /// Текущее количество снарядов оружия.
        /// </summary>
        public int BulletsCount
        {
            get { return _bulletsCounter; }
        }

        /// <summary>
        /// Ссылка на префаб снаряда.
        /// </summary>
        [Tooltip("Префаб снаряда")]
        public Bullet BulletPrefab;

        /// <summary>
        /// Точки появления снарядов и вспышек оружия.
        /// </summary>
        [Tooltip("Точки появления снарядов и вспышек оружия")]
        public Transform[] StartPoints;

        /// <summary>
        /// Указывает включается ли автоматическая перезарядка, или требуется подбор точки восстановления боезапаса.
        /// </summary>
        [Tooltip("Указывает включается ли автоматическая перезарядка, или требуется подбор точки восстановления боезапаса")]
        public bool AutoReload = false;

        /// <summary>
        /// Время перезарядки.
        /// </summary>
        [Tooltip("Время перезарядки")]
        public int TimeReload;

        private float _shotTime = 0.0f;

        private bool _reloading = false;

        private Transform _bulletCollector;

        void Start()
        {
            GameObject bulletCollector = GameObject.FindGameObjectWithTag("BulletCollector");
            if (bulletCollector == null)
            {
                bulletCollector = new GameObject("BulletCollector");
                bulletCollector.tag = "BulletCollector";
            }
            _bulletCollector = bulletCollector.transform;
            _rateOfFire = 60.0f / RateOfFire;

            _bulletsCounter = MaxBulletsMagazine;
        }

        /// <summary>
        /// Метод реализует стрельбу.
        /// </summary>
        /// <param name="triggered">Указывает нажата ли кнопка стрельбы.</param>
        /// <returns>Возвращает true, если выстрел действительно произведён.</returns>
        public bool Fire(bool triggered)
        {
            bool result = false;
            if (!triggered)
            {
                _untriggered = true;
                return false;
            }
            bool isFire = true;
            if (FireType != EFireType.Auto)
            {
                isFire = _untriggered;
                if (_untriggered)
                    _untriggered = false;
            }
            if (isFire)
            {
                result = true;
                if ((Time.time > _shotTime) && (!_reloading) && _bulletsCounter > 0)
                {
                    _shotTime = Time.time + _rateOfFire;
                    if (FireType == EFireType.Full)
                    {
                        MakeBullet();
                        int counterDefferedBullets = _bulletsCounter;
                        const float periodAdd = 0.25f;
                        float period = periodAdd;
                        while (counterDefferedBullets > 0)
                        {
                            int countPoints = StartPoints.Length;
                            if (countPoints > _bulletsCounter)
                                countPoints = _bulletsCounter;
                            for (int i = 0; i < countPoints; i++)
                            {
                                StartCoroutine(MakeBulletWithPeriod(period));
                                period += periodAdd;
                                counterDefferedBullets--;
                            }
                        }
                    } else 
                        MakeBullet();
                }
            }
            if (_bulletsCounter == 0 && AutoReload)
                _bulletsCounter = MaxBulletsMagazine;
            return result;
        }

        /// <summary>
        /// Метод реализует создание снарядов через указанный промежуток времени.
        /// </summary>
        /// <param name="period">Время, через которое необходимо создать снаряд на сцене.</param>
        /// <returns></returns>
        private IEnumerator MakeBulletWithPeriod(float period)
        {
            yield return new WaitForSeconds(period);
            MakeBullet();
        }

        /// <summary>
        /// Метод реализует непосредственно создание снаряда на сцене.
        /// </summary>
        /// <returns>Возвращает объект созданного снаряда.</returns>
        private Bullet MakeBullet()
        {
            Vector3 point = StartPoints[Random.Range(0, StartPoints.Length)].position;
            Bullet bullet = PoolManager.Instance.PopOrCreate<Bullet>(BulletPrefab, point, transform.rotation);
            bullet.transform.parent = _bulletCollector;
            bullet.Direction = transform.forward;
            bullet.StartPoint = point;
            _bulletsCounter--;
            return bullet;
        }

        /// <summary>
        /// Восстанавливает боезапас оружия.
        /// </summary>
        public void RestoreBullets()
        {
            _bulletsCounter = MaxBulletsMagazine;
        }
    }
}