﻿using System.Collections;
using RedDev.Gameplay.Characters;
using RedDev.Gameplay.Characters.Enemies;
using RedDev.Gameplay.Characters.Tank;
using RedDev.Gameplay.Helpers;
using RedDev.ManagersEngine;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RedDev.Gameplay
{
    /// <summary>
    /// Класс реализует фабрику создания противников.
    /// </summary>
    public class EnemyManfunact : BaseManufact<EnemyState>
    {
        private TankState _stateTank;

        void Start()
        {
            GameObject gobjPlayer = GameObject.FindGameObjectWithTag("Player");
            if (gobjPlayer == null)
            {
                Debug.LogError("Для " + gameObject.name + " не был найден объект с тегом Player");
            }
            else
            {
                _stateTank = gobjPlayer.GetComponent<TankState>();
                if (_stateTank == null)
                    Debug.LogError("Не был найден StateTank для" + gameObject.name);
            }

            for (int i = 0; i < MaxActiveRespawners; i++)
                StartRespawn();
        }

        void FixedUpdate()
        {
            
        }

        protected override IEnumerator StartWaiting(Transform transformPoint)
        {
            if (ObjectsPrefabs.Length == 0)
                yield break;
            yield return new WaitForSeconds(TimeForRespawn);
            int index = Random.Range(0, ObjectsPrefabs.Length);
            EnemyState activePoint = PoolManager.Instance.PopOrCreate(ObjectsPrefabs[index]);
            activePoint.transform.position = transformPoint.position;

            Health health = activePoint.GetComponent<Health>();
            if ((health != null) && activePoint.FirstInitialize)
            {
                health.OnDead.AddListener(SendKillStat);
                health.OnDead.AddListener(StartRespawn);
                activePoint.FirstInitialize = false;
            }
            yield return base.StartWaiting(transformPoint);
        }

        private void SendKillStat()
        {
            _stateTank.AddKill();
        }
    }
}