﻿using UnityEngine;

namespace RedDev.Gameplay.Icons
{
    /// <summary>
    /// Класс описывает анимацию вращения объекта круга.
    /// </summary>
    public class CircleAnimation : MonoBehaviour
    {
        private Transform _circle;

        /// <summary>
        /// Скорость вращения объекта.
        /// </summary>
        public float Speed;

        /// <summary>
        /// Оси вращения круга.
        /// </summary>
        public Vector3 Axis;
        
        void Start()
        {
            _circle = transform.Find("circle_icon");
            if (_circle == null)
                Debug.LogError("У иконки оружия отсутствует дочерний объект circle_icon");
        }
        
        void Update()
        {
            _circle.Rotate(Axis * Speed * Time.deltaTime);
        }
    }
}