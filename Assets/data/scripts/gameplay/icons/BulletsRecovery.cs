﻿using RedDev.Helpers;
using UnityEngine;
using UnityEngine.Events;

namespace RedDev.Gameplay.Icons
{
    /// <summary>
    /// Класс описывает точку восстановления боеприпасов.
    /// </summary>
    public class BulletsRecovery : ObjectPool
    {
        /// <summary>
        /// Название оружия, которому необходимо восстановить снаряды при подброе данной точки.
        /// </summary>
        public string WeaponName;

        /// <summary>
        /// События, которые должны выполниться при подборе данной точки.
        /// </summary>
        public UnityEvent EventTrigger;

        /// <summary>
        /// Вероятность выпадения именно данной точки.
        /// </summary>
        public int RespawnChance;

        /// <summary>
        /// Поле необходимо для фабрики, указывает что необходима первинчная настройка.
        /// </summary>
        [HideInInspector]
        public bool FirstInitialize = true;
    }
}