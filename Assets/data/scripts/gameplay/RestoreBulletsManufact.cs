﻿using System.Collections;
using RedDev.Gameplay.Helpers;
using RedDev.ManagersEngine;
using RedDev.Gameplay.Icons;
using UnityEngine;

namespace RedDev.Gameplay
{
    /// <summary>
    /// Класс реализует фабрику создания объектов восстановления ресурсов игрока.
    /// </summary>
    public class RestoreBulletsManufact : BaseManufact<BulletsRecovery>
    {
        void Start()
        {
            for (int i = 0; i < MaxActiveRespawners; i++)
                StartRespawn();
        }

        protected override IEnumerator StartWaiting(Transform transformPoint)
        {
            if (ObjectsPrefabs.Length == 0)
                yield break;
            yield return new WaitForSeconds(TimeForRespawn);
            int index = Random.Range(0, ObjectsPrefabs.Length);
            BulletsRecovery activePoint = PoolManager.Instance.PopOrCreate(ObjectsPrefabs[index]);

            if ((activePoint.EventTrigger != null) && activePoint.FirstInitialize)
            {
                activePoint.EventTrigger.AddListener(activePoint.Push);
                activePoint.EventTrigger.AddListener(StartRespawn);
                activePoint.FirstInitialize = false;
            }
            activePoint.transform.position = transformPoint.position;
            yield return base.StartWaiting(transformPoint);
        }
    }
}