﻿using RedDev.Gameplay.Characters.Tank;
using UnityEngine;

namespace RedDev.Gameplay.Camera
{
    public class CameraTopDown : MonoBehaviour
    {
        private TankState _state;

        [Tooltip("Объект слежения камеры")]
        public Transform Target;

        [Tooltip("Скорость перемещения камеры за объектом слежения")]
        public float SpeedMovement = 1.0f;

        [Tooltip("Минимальная высота камеры")]
        public float MinHeight = 7.0f;

        [Tooltip("Максимальная высота камеры")]
        public float MaxHeight = 14.0f;

        [Tooltip("Скорость изменения высоты камеры")]
        public float SpeedZoom = 2.0f;

        private float _currentHeight = 5.0f;

        // Use this for initialization
        void Start()
        {
            _currentHeight = transform.position.y;
        }

        // Update is called once per frame
        void Update()
        {
            float zoom = _currentHeight;

            Vector3 pos = new Vector3(Target.position.x, zoom, Target.position.z);
            transform.position = Vector3.Lerp(transform.position, pos, SpeedMovement * Time.deltaTime);
        }
    }
}