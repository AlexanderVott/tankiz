﻿using RedDev.Gameplay.Characters.Tank;
using UnityEngine;

namespace RedDev.Gameplay
{
    /// <summary>
    /// Класс реализует управление танком пользователем.
    /// </summary>
    [RequireComponent(typeof(TankState))]
    public class UserControl : MonoBehaviour
    {
        private float _inputMovement;
        private float _inputTurn;

        private TankState _state;

        void Start()
        {
            _state = GetComponent<TankState>();
        }

        void Update()
        {
            _inputMovement = Input.GetAxis("Vertical");
            _inputTurn = Input.GetAxis("Horizontal");
            _state.Moving(_inputMovement);
            _state.Turning(_inputTurn);
            _state.Fire(Input.GetButton("Fire1"));
            
            //TODO: слишком быстрое переключение.
            if (Input.GetButtonDown("SwitchWeapon"))
            {
                float switchWeapon = Input.GetAxis("SwitchWeapon");
                if (switchWeapon >= 0.1f)
                    _state.WeaponController.NextWeapon();
                else if (switchWeapon <= -0.1f)
                    _state.WeaponController.PrevWeapon();
            }
        }
    }
}