﻿using UnityEngine;

namespace RedDev.Helpers
{
	/// <summary>
	/// Описывает момент изменения значения действия.
	/// </summary>
	public enum ActionCompleteType {
		/// <summary>
		/// Меняет значение на время выполнения действия.
		/// </summary>
		ChangeInAction,

		/// <summary>
		/// Меняет значение в начале действия, не изменяя его обратно по завершении.
		/// </summary>
		ChangeOnEnter,

		/// <summary>
		/// Меняет значение в конце действия.
		/// </summary>
		ChangeOnExit
	}

	/// <summary>
	/// Описывает изменение переменных animator'а в процессе выполнения действий.
	/// </summary>
	public class ActionComplete : StateMachineBehaviour {
		/// <summary>
		/// Идентификатор параметра.
		/// </summary>
		private int _paramId = -1;

		/// <summary>
		/// Название параметра.
		/// </summary>
		public string ParamComplete;

		/// <summary>
		/// Тип изменения параметра.
		/// </summary>
		public ActionCompleteType TypeAction = ActionCompleteType.ChangeInAction;

		/// <summary>
		/// Значение, которое будет присвоено в соответствии с выбранным типом изменения.
		/// </summary>
		public bool Value = true;

		// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			// Находим идентификатор параметра, или присваиваем код ошибки.
			if (_paramId == -2)
				return;
			if (_paramId == -1) {
				if (ParamComplete.Length == 0)
					_paramId = -2;
				else
					_paramId = Animator.StringToHash(ParamComplete);
			}
			// Если тип действия OnEnter или InAction меняем значение сразу.
			if ((TypeAction == ActionCompleteType.ChangeInAction) || (TypeAction == ActionCompleteType.ChangeOnEnter))
				animator.SetBool(ParamComplete, Value);
		}

		// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
			if (_paramId == -2)
				return;
			// Если тип действия InAction - меняем значение при выходе на обратное.
			// Если OnExit - меняем на указанное значение при выходе.
			if (TypeAction == ActionCompleteType.ChangeInAction)
				animator.SetBool(ParamComplete, !Value);
			else
				if (TypeAction == ActionCompleteType.ChangeOnExit)
					animator.SetBool(ParamComplete, Value);
		}
	}
}
