﻿using System;
using UnityEngine.Events;

namespace RedDev.Helpers
{
    /// <summary>
    /// Класс реализует событие unity принимаемое в качестве параметра целочисленное значение.
    /// </summary>
    [Serializable]
    public class EventInt : UnityEvent<int>
    {
    }
}