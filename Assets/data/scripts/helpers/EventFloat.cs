﻿using System;
using UnityEngine.Events;

namespace RedDev.Helpers
{
    /// <summary>
    /// Класс реализует событие unity принимаемое в качестве параметра число с плавающей запятой.
    /// </summary>
    [Serializable]
    public class EventFloat : UnityEvent<float>
    {
    }
}