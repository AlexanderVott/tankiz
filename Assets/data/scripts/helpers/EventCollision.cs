﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace RedDev.Helpers
{
    /// <summary>
    /// Класс реализует событие unity принимаемое в качестве параметра коллизию.
    /// </summary>
    [Serializable]
    public class EventCollision : UnityEvent<Collision>
    {
    }
}